<?php
/**
 *
 * @var $player \app\mapper\User
 */
$players = $this->players;
?>
<table>
    <th>ID</th>
    <th>prename</th>
    <th>lastname</th>
    <th>email</th>
    <?php
    foreach ($players as $player) {
        ?>
        <tr>
            <td><?php echo $player->getId(); ?></td>
            <td><?php echo $player->getPrename(); ?></td>
            <td><?php echo $player->getName(); ?></td>
            <td><?php echo $player->getMail(); ?></td>
        </tr>
        <?php
    }
    ?>
</table>