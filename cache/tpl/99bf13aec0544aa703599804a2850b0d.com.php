<?php
/**
 *
 * @var $players \app\mapper\User[]
 */
$players = $this->players;
?>

<h1>Users</h1>

<table>
    <th>ID</th>
    <th>prename</th>
    <th>lastname</th>
    <th>email</th>
    <?php
    foreach ($players as $player) {
        ?>
        <tr>
            <td><?php echo $player->getId(); ?></td>
            <td><?php echo $player->getPrename(); ?></td>
            <td><?php echo $player->getName(); ?></td>
            <td><?php echo $player->getMail(); ?></td>
        </tr>
        <?php
    }
    ?>
</table>

<form action="index.php" method="POST">
    <input type="hidden" name="c" value="Main">
    <input type="hidden" name="m" value="create">
    prename: 
    <input name="prename" type="text" value=""/>
    name: 
    <input name="name" type="text" value=""/>
    mail: 
    <input name="mail" type="text" value=""/>
    <input type="submit" value="save"/>
</form>