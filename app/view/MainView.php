<?php

namespace app\view;

use rueckgrat\mvc\FastView;

/**
 * Description of MainView
 *
 * @author Sale
 */
class MainView extends FastView {

    /**
     *
     * @var $players \app\mapper\User[]
     */
    protected $players;
    protected $table;

    public function __construct() {
        parent::__construct();
        $this->cacheDisabled = TRUE;
        $this->title = "hello framework";
    }

    public function renderFrontPage($players) {
        $this->players = $players;

        $this->pageContent = $this->getCompiledTpl('main/main');

        return $this->renderMainPage();
    }

}

?>
