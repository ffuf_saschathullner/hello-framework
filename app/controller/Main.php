<?php

namespace app\controller;

use rueckgrat\mvc\DefaultController;
use app\model\UserModel;
use app\view\MainView;
use rueckgrat\exceptions\InputException;
use rueckgrat\security\Input;

/**
 * Description of Main
 *
 * @author Sale
 */
class Main extends DefaultController {

    /**
     *
     * @var app\view\MainView
     */
    protected $mainView;

    /**
     *
     * @var app\model\UserrModel
     */
    protected $userModel;

    public function __construct() {
        parent::__construct();
        $this->userModel = new UserModel();
        $this->mainView = new MainView();
    }

    public function index() {
        $players = $this->userModel->getAllUsers();

        return $this->mainView->renderFrontPage($players);
    }

    public function create() {
        $user = new \app\mapper\User();
        $user->map(array(
            'prename' => Input::p('prename'),
            'name' => Input::p('name'),
            'mail' => Input::p('mail')
        ));

        $this->userModel->createUser($user);

        header("Location: ?c=Main&m=index");
    }

}

?>
