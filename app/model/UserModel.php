<?php

namespace app\model;

use \rueckgrat\mvc\DefaultDBModel;

/**
 * Description of UserModel
 *
 * @author Sale
 */
class UserModel extends DefaultDBModel {

    protected $validator;

    public function __construct() {
        parent::__construct("user");
        $this->validator = new \rueckgrat\security\InputValidator();
    }

    public function getAllUsers() {
        $stmnt = $this->db->query('SELECT * FROM user');
        $players = array();

        while ($row = $stmnt->fetch()) {
            $player = new \app\mapper\User();
            $player->map($row);
            $players[] = $player;
        }

        return $players;
    }

    public function createUser($user) {
        $this->validator->validate(new \app\validator\UserValidator($user));
        $this->create($user);
    }

}

?>
